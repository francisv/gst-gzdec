/* GStreamer
 * Copyright (C) 2018 Francissco Javier Velazquez-Garcia <francisv@ifi.uio.no>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstgzdec
 *
 * The gzdec element decompresses GNU Gzip files
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v filesrc location=file.txt.gz ! gzdec method=0 ! filesink location="file.txt"
 * ]|
 * This pipeline is equivalent to
 * |[
 * gunzip -c file.txt.gz > file.txt
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>
#include "gstgzdec.h"

GST_DEBUG_CATEGORY_STATIC (gst_gzdec_debug_category);
#define GST_CAT_DEFAULT gst_gzdec_debug_category

/* prototypes */
static void gst_gzdec_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_gzdec_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_gzdec_dispose (GObject * object);
static void gst_gzdec_finalize (GObject * object);

static gboolean gst_gzdec_decide_allocation (GstBaseTransform * trans,
    GstQuery * query);
static gboolean gst_gzdec_filter_meta (GstBaseTransform * trans,
    GstQuery * query, GType api, const GstStructure * params);
static gboolean gst_gzdec_propose_allocation (GstBaseTransform * trans,
    GstQuery * decide_query, GstQuery * query);
static gboolean gst_gzdec_start (GstBaseTransform * trans);
static gboolean gst_gzdec_stop (GstBaseTransform * trans);
static gboolean gst_gzdec_sink_event (GstBaseTransform * trans,
    GstEvent * event);
static gboolean gst_gzdec_src_event (GstBaseTransform * trans,
    GstEvent * event);
static GstFlowReturn gst_gzdec_prepare_output_buffer (GstBaseTransform *
    trans, GstBuffer * input, GstBuffer ** outbuf);
static gboolean gst_gzdec_copy_metadata (GstBaseTransform * trans,
    GstBuffer * input, GstBuffer * outbuf);
static gboolean gst_gzdec_transform_meta (GstBaseTransform * trans,
    GstBuffer * outbuf, GstMeta * meta, GstBuffer * inbuf);
static void gst_gzdec_before_transform (GstBaseTransform * trans,
    GstBuffer * buffer);
static GstFlowReturn gst_gzdec_transform (GstBaseTransform * trans,
    GstBuffer * inbuf, GstBuffer * outbuf);
static GstFlowReturn gst_gzdec_transform_ip (GstBaseTransform * trans,
    GstBuffer * buf);
static gboolean gst_gzdec_decode_gzip_stream (z_stream * stream,
    guint8 * indata, gsize insize, guint8 ** outdata, gsize * outsize,
    guint blocksize);
static gboolean gst_gzdec_decode_bzip_stream (bz_stream * stream,
    guint8 * indata, gsize insize, guint8 ** outdata, gsize * outsize,
    guint blocksize);

enum
{
  PROP_0,
  PROP_METHOD
};

GType
gst_gzdec_method_get_type (void)
{
  static GType gzdec_method_type = 0;
  static const GEnumValue gzdec_method[] = {
    {GST_GZDEC_METHOD_GZIP, "Gzip decompression algorithm", "gzip"},
    {GST_GZDEC_METHOD_BZIP2, "bzip2 decompression algorithm", "bzip2"},
    {0, NULL, NULL},
  };

  if (!gzdec_method_type)
    gzdec_method_type = g_enum_register_static ("GstGzDecMethod", gzdec_method);

  return gzdec_method_type;
}

/* pad templates */

static GstStaticPadTemplate gst_gzdec_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate gst_gzdec_sink_template =
    GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
                             /*Add guards*/
    GST_STATIC_CAPS ("application/x-gzip; application/x-bzip")
    );


/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstGzdec, gst_gzdec, GST_TYPE_BASE_TRANSFORM,
    GST_DEBUG_CATEGORY_INIT (gst_gzdec_debug_category, "gzdec", 0,
        "debug category for gzdec element"));

static void
gst_gzdec_class_init (GstGzdecClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstBaseTransformClass *base_transform_class =
      GST_BASE_TRANSFORM_CLASS (klass);

  gst_element_class_add_static_pad_template (GST_ELEMENT_CLASS (klass),
      &gst_gzdec_src_template);
  gst_element_class_add_static_pad_template (GST_ELEMENT_CLASS (klass),
      &gst_gzdec_sink_template);

  gst_element_class_set_static_metadata (GST_ELEMENT_CLASS (klass),
      "GNU Gzip decoder", "Decoder",
      "Decodes compressed streams in GNU Gzip format",
      "Francisco Javier Velazquez-Garcia <francisv@ifi.uio.no>");

  gobject_class->set_property = gst_gzdec_set_property;
  gobject_class->get_property = gst_gzdec_get_property;
  gobject_class->dispose = gst_gzdec_dispose;
  gobject_class->finalize = gst_gzdec_finalize;
  base_transform_class->decide_allocation =
      GST_DEBUG_FUNCPTR (gst_gzdec_decide_allocation);
  base_transform_class->filter_meta = GST_DEBUG_FUNCPTR (gst_gzdec_filter_meta);
  base_transform_class->propose_allocation =
      GST_DEBUG_FUNCPTR (gst_gzdec_propose_allocation);
  base_transform_class->start = GST_DEBUG_FUNCPTR (gst_gzdec_start);
  base_transform_class->stop = GST_DEBUG_FUNCPTR (gst_gzdec_stop);
  base_transform_class->sink_event = GST_DEBUG_FUNCPTR (gst_gzdec_sink_event);
  base_transform_class->src_event = GST_DEBUG_FUNCPTR (gst_gzdec_src_event);
  base_transform_class->prepare_output_buffer =
      GST_DEBUG_FUNCPTR (gst_gzdec_prepare_output_buffer);
  base_transform_class->copy_metadata =
      GST_DEBUG_FUNCPTR (gst_gzdec_copy_metadata);
  base_transform_class->transform_meta =
      GST_DEBUG_FUNCPTR (gst_gzdec_transform_meta);
  base_transform_class->before_transform =
      GST_DEBUG_FUNCPTR (gst_gzdec_before_transform);
  base_transform_class->transform = GST_DEBUG_FUNCPTR (gst_gzdec_transform);
  base_transform_class->transform_ip =
      GST_DEBUG_FUNCPTR (gst_gzdec_transform_ip);

  /* define properties */
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_METHOD,
      g_param_spec_enum ("method", "Decoding method",
          "The decoding method to use", GST_TYPE_GZDEC_METHOD,
          GST_GZDEC_METHOD_GZIP, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

static void
gst_gzdec_init (GstGzdec * gzdec)
{
  /* Not needed, because GObject evaluates*/
  gzdec->method = 0;
}

void
gst_gzdec_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstGzdec *gzdec = GST_GZDEC (object);

  GST_DEBUG_OBJECT (gzdec, "set_property");

  switch (property_id) {
    case PROP_METHOD:
      gzdec->method = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_gzdec_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstGzdec *gzdec = GST_GZDEC (object);

  GST_DEBUG_OBJECT (gzdec, "get_property");

  switch (property_id) {
    case PROP_METHOD:
      g_value_set_enum (value, gzdec->method);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_gzdec_dispose (GObject * object)
{
  GstGzdec *gzdec = GST_GZDEC (object);

  GST_DEBUG_OBJECT (gzdec, "dispose");

  /* clean up as possible.  may be called multiple times */

  G_OBJECT_CLASS (gst_gzdec_parent_class)->dispose (object);
}

void
gst_gzdec_finalize (GObject * object)
{
  GstGzdec *gzdec = GST_GZDEC (object);

  GST_DEBUG_OBJECT (gzdec, "finalize");

  /* clean up object here */

  G_OBJECT_CLASS (gst_gzdec_parent_class)->finalize (object);
}

/* decide allocation query for output buffers */
static gboolean
gst_gzdec_decide_allocation (GstBaseTransform * trans, GstQuery * query)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "decide_allocation");

  return TRUE;
}

static gboolean
gst_gzdec_filter_meta (GstBaseTransform * trans, GstQuery * query, GType api,
    const GstStructure * params)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "filter_meta");

  return TRUE;
}

/* propose allocation query parameters for input buffers */
static gboolean
gst_gzdec_propose_allocation (GstBaseTransform * trans,
    GstQuery * decide_query, GstQuery * query)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "propose_allocation");

  return TRUE;
}

/* states */
static gboolean
gst_gzdec_start (GstBaseTransform * trans)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "start");

  switch (gzdec->method) {
    case GST_GZDEC_METHOD_GZIP:
      /* Init the GZip stream */
      /* Free memory in gst_gzdec_stop */
      gzdec->gz_stream.zalloc = (alloc_func) 0;
      gzdec->gz_stream.zfree = (free_func) 0;
      gzdec->gz_stream.opaque = (voidpf) 0;
      if (Z_OK != inflateInit2 (&gzdec->gz_stream, 32))
        return FALSE;
      break;
    case GST_GZDEC_METHOD_BZIP2:
      gzdec->bz_stream.bzalloc = NULL;
      gzdec->bz_stream.bzfree = NULL;
      gzdec->bz_stream.opaque = NULL;
      if (BZ_OK != BZ2_bzDecompressInit (&gzdec->bz_stream, 0, 0))
        return FALSE;
      break;
    default:
      GST_ELEMENT_ERROR (gzdec, CORE, FAILED, (NULL), ("Unexpected method"));
  }

  return TRUE;
}

static gboolean
gst_gzdec_stop (GstBaseTransform * trans)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "stop");

  switch (gzdec->method) {
    case GST_GZDEC_METHOD_GZIP:
      inflateEnd (&gzdec->gz_stream);
      break;
    case GST_GZDEC_METHOD_BZIP2:
      BZ2_bzDecompressEnd (&gzdec->bz_stream);
      break;
    default:
      GST_ELEMENT_ERROR (gzdec, CORE, FAILED, (NULL), ("Unexpected method"));
  }

  return TRUE;
}

/* sink and src pad event handlers */
static gboolean
gst_gzdec_sink_event (GstBaseTransform * trans, GstEvent * event)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "sink_event");

  return GST_BASE_TRANSFORM_CLASS (gst_gzdec_parent_class)->sink_event (trans,
      event);
}

static gboolean
gst_gzdec_src_event (GstBaseTransform * trans, GstEvent * event)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "src_event");

  return GST_BASE_TRANSFORM_CLASS (gst_gzdec_parent_class)->src_event (trans,
      event);
}

static gboolean
gst_gzdec_decode_gzip_stream (z_stream * stream, guint8 * indata, gsize insize,
    guint8 ** outdata, gsize * outsize, guint blocksize)
{
  guint8 *out = (guint8 *) g_malloc_n (blocksize, sizeof (guint8));
  if (!out)
    return FALSE;

  /* Inflate */
  size_t out_size = 0;
  size_t offset = 0;
  stream->avail_in = insize;
  stream->next_in = indata;
  do {
    stream->avail_out = blocksize;
    stream->next_out = out + offset;
    const int res = inflate (stream, Z_NO_FLUSH);
    /* Error messages from zutil.c */
    switch (res) {
      case Z_NEED_DICT:
        GST_ERROR ("Unexpected dictionary");
      case Z_DATA_ERROR:
        GST_ERROR ("GZ data error");
      case Z_MEM_ERROR:
        GST_ERROR ("GZ could not allocate enough memory.");
        g_free (out);
        return FALSE;
      default:
        break;
    }
    size_t have = blocksize - stream->avail_out;
    out_size += have;
    offset += blocksize;
    if (stream->avail_out == 0) {
      out = (guint8 *) g_realloc (out, (blocksize + offset) * sizeof (guint8));
      if (!out) {
        g_free (out);
        return FALSE;
      }
    }
  } while (stream->avail_out == 0);

  /* Set outdata and outsize */
  *outdata = out;
  *outsize = out_size;
  return TRUE;
}

static gboolean
gst_gzdec_decode_bzip_stream (bz_stream * stream, guint8 * indata, gsize insize,
    guint8 ** outdata, gsize * outsize, guint blocksize)
{
  /* Allocate the output buffer */
  guint8 *out = (guint8 *) g_malloc_n (blocksize, sizeof (guint8));
  if (!out)
    return FALSE;

  /* Decompress */
  size_t out_size = 0;
  size_t offset = 0;
  stream->avail_in = insize;
  stream->next_in = (char *) indata;
  do {
    stream->avail_out = blocksize;
    stream->next_out = (char *) (out + offset);
    const int res = BZ2_bzDecompress (stream);
    switch (res) {
      case BZ_MEM_ERROR:
        GST_ERROR ("BZ2 could not allocate enough memory.");
      case BZ_DATA_ERROR:
        GST_ERROR ("BZ data integrity (CRC) error in data");
      case BZ_DATA_ERROR_MAGIC:
        GST_ERROR ("Bad magit number (file not created by bzip2)");
      case BZ_PARAM_ERROR:
        g_free (out);
      default:
        break;
    }
    size_t have = blocksize - stream->avail_out;
    out_size += have;
    offset += blocksize;
    if (stream->avail_out == 0) {
      out = (guint8 *) g_realloc (out, (blocksize + offset) * sizeof (guint8));
      if (!out) {
        g_free (out);
        return FALSE;
      }
    }
  } while (stream->avail_out == 0);

  /* Set outdata and outsize */
  *outdata = (guint8 *) out;
  *outsize = out_size;
  return TRUE;
}

static GstFlowReturn
gst_gzdec_prepare_output_buffer (GstBaseTransform * trans, GstBuffer * input,
    GstBuffer ** outbuf)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "prepare_output_buffer");

  /* Get the input buffer map */
  GstMapInfo in_map;
  if (!gst_buffer_map (input, &in_map, GST_MAP_READ)) {
    GST_ERROR ("failed to map input buffer\n");
    return GST_FLOW_ERROR;
  }

  /* Decode the data */
  guint8 *outdata = NULL;
  gsize outsize = 0;
  gboolean res = FALSE;
  switch (gzdec->method) {
    case GST_GZDEC_METHOD_GZIP:
      res =
          gst_gzdec_decode_gzip_stream (&gzdec->gz_stream, in_map.data,
          in_map.size, &outdata, &outsize, in_map.size);
      break;

    case GST_GZDEC_METHOD_BZIP2:
      res =
          gst_gzdec_decode_bzip_stream (&gzdec->bz_stream, in_map.data,
          in_map.size, &outdata, &outsize, in_map.size);
      break;

    default:
      GST_ERROR ("decoding method not implemented\n");
      gst_buffer_unmap (input, &in_map);
      return GST_FLOW_ERROR;
  }
  if (!res) {
    GST_ERROR ("failed to decode stream\n");
    gst_buffer_unmap (input, &in_map);
    return GST_FLOW_ERROR;
  }

  /* Allocate the output buffer */
  *outbuf = gst_buffer_new_wrapped (outdata, outsize);
  if (!*outbuf) {
    GST_ERROR ("failed to allocate output buffer\n");
    gst_buffer_unmap (input, &in_map);
    return GST_FLOW_ERROR;
  }

  gst_buffer_unmap (input, &in_map);

  return GST_FLOW_OK;
}

/* metadata */
static gboolean
gst_gzdec_copy_metadata (GstBaseTransform * trans, GstBuffer * input,
    GstBuffer * outbuf)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "copy_metadata");

  return TRUE;
}

static gboolean
gst_gzdec_transform_meta (GstBaseTransform * trans, GstBuffer * outbuf,
    GstMeta * meta, GstBuffer * inbuf)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "transform_meta");

  return TRUE;
}

static void
gst_gzdec_before_transform (GstBaseTransform * trans, GstBuffer * buffer)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "before_transform");

}

/* transform */
static GstFlowReturn
gst_gzdec_transform (GstBaseTransform * trans, GstBuffer * inbuf,
    GstBuffer * outbuf)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "transform");

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_gzdec_transform_ip (GstBaseTransform * trans, GstBuffer * buf)
{
  GstGzdec *gzdec = GST_GZDEC (trans);

  GST_DEBUG_OBJECT (gzdec, "transform_ip");

  return GST_FLOW_OK;
}
